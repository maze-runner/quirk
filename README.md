What is Unit Testing?

Unit Testing is a method of testing the smallest piece of code called a unit. The main goal is to validate that each unit of the software performs as expected.
The most important thing in unit testing is not to create test cases for everything, but to focus on the tests that affect the behaviour of the system.

Why Unit Testing?
Improve quality of code.
Detects software bugs on new changes.
Provides a more reliable codebase.
Facilitates changes so speed up the coding part.

Types of Unit testing :
There are mainly two types of unit testing that are being followed :
Black Box testing - In this kind of testing we are hiding the functionality of a method/component from the user. We are testing the functions inputs and output via interface only. We don’t need to know about internal working.
White Box testing -  It also tests how the output is achieved not only what is the output. It is a testing of behaviour rather than just interface, input and output.
The code coverage is the example of white box testing. 
Statement Coverage - This technique is aimed at exercising all programming statements with minimal tests.
Branch Coverage - This technique is running a series of tests to ensure that all branches are tested at least once.
Path Coverage - This technique corresponds to testing all possible paths which means that each statement and branch is covered.

For example - 
Let us assume f(x) = a * b + c
Let a = 2, b=3, c =4
In black box testing we will put above values in f(x) and we expect the output to be 10. In this case we don’t want to know how we achieved 10, we just know our expected result is equal to actual so the test is passed.
In the white box we need to know how the function is behaving. As what a * b would do what the `+` sign would do.  And we reach to the conclusion using the behaviour results top of the test.

The approach of developing applications side by side with testing is called test driven development. This approach is highly adapted these days. TDD (Test Driven Development) is an approach rather than a rigid framework. The order of development is - 

Unit testing ---> Integration testing -----> System testing


Now the question is what is the smallest unit of code?
The definition is vague out of necessity: It’s up to the developers to decide what the unit of code is. In object oriented language it’s usually a method. But in C developers have to decode what method needs to be tested actually. But the most basic thing is unit tests should be as simple as possible.

Now if we are unit testing a method, we don’t need to verify the internal details of the method, we are only testing the interface. 
This is because of a very simple reason: the internal implementation of the method can change but as far as the expected behaviour doesn’t change there is nothing to worry.

How is it done ? 
Here I am not considering any framework for example. Just taking an common example for C - 
testRunner.c
#include<stdio.h>
void  mytestFunc1() {
    test(sum(4,6), 10);
} 
void  mytestFunc2(int param) {
    test(sum(1,2), 3);
    test(mul(3,2), 5);
} 
void  mytestFunc3(){
    test(mul(3,3), 9);
}

Int main() {

	/* Running both tests separately */
	RUN_TEST(mytestFunc1);  // RUN_TEST will execute the test functions
	RUN_TEST(mytestFunc3);

	/* running tests in one */
	RUN_TEST(mytestFunc2);
}


Now you may have noticed that I have written two separate tests for two methods. Nothing is stopping us from testing both methods in a single test. But that can be an issue.
What happens if one test fails and one passes? The test will fail completely and we’ll never know which method caused it to fail. That’s why we prefer a single assertion per test.
For example in above code mytestFunc2() test will fail because mul() function’s expected and actual values are different. But as we have written sum and mul in one test we will never know which function is causing failing tests. 
So a well structured Unit tests give you one less thing to worry about while writing code.
As we can’t know for sure that we have covered all cases, here comes the manual testing part.

Problems:
Unit testing is highly affected by the architecture of the application. This means that for different languages (javascript, python, golang etc.) the approach of writing unit tests varies. 
Too many dependencies are mocked; it's a sign the module has too many responsibilities.  It is a symptom of too tight coupling between modules. Like we have to be dependent on another component to test the behaviour of a component.

Things not included as the part of Unit testing :

3rd party Libraries and frameworks - There are several unit testing frameworks for different languages. If we just consider the C language for example, there are testing libraries googletest, checker, Unity, CppUnit etc. Now if we are using one of these libraries in our project we shouldn’t be testing there inbuilt methods in our application. If you have doubt regarding any function/method used don’t use that framework or report to that organization but don’t try to test them. It just increases work load unnecessarily.
Trivial code (like getters and setters) - In object oriented programming we use this a lot. We use setter to set the value of any variable, and getter to get the value of that variable when they are defined as private. So we don’t need to write tests for them, but we will write tests on their use cases though.  
Code that has non-deterministic results. If a function’s output is non-deterministic, it means we sent any request to the server and the response keeps updating, so we can’t test it. We can just test the response code if it’s ok or not. Means if response code returned from server is 200 or other.
Environment and config files. The environment files build at the run time, so we can’t test them at the compile time. So we can’t test the environment and config files.
The unit test should not talk to the database. It can when it is highly needed, but it comes with the trade off. If you are making your test cases talk to the database then it will slow down the testing speed.
A unit test shouldn’t touch the file system. Now the term file system is not a clear term. So for explanation, When you have to load a database or a set of files separately from the test suite, the test cases may be fragile. A reliable test case has exactly one reason to fail, and that reason has nothing to do with whether an external database or file has been loaded correctly. It means that we are not interested in testing whether the SELECT statement is compatible with oracle or sql. We want to test the result coming from that statement. If our test fails due to database issues, then it’s fragile.
If any test is blocking other tests, means only one test can run at a time due to dependencies issues it shouldn’t be there. As it will increase the time for unit testing our system a lot.
Tests that do the above things aren't bad. Often they are worth writing, and they can be written in a unit test harness. However, it is important to be able to separate them from true unit tests so that we can keep a set of tests that we can run fast whenever we make our changes.

Some points:

How fast should a unit test be? 
Now this question is tricky. If the project has special requirements which will need time, and are required then the test time will increase. Here is an answer from stackoverflow https://stackoverflow.com/a/10513
 A test should run 100s tests per second.
How often should a unit test be run?
Before jumping to how often we should run unit tests, we need to know the FIRST(Fast, Isolates, Repeatable,Self Validating, Timely) property of unit tests.
It should be possible to run the unit tests quickly, so that you can run them after every simple change.
Tests must be fast. If you hesitate to run the tests after a simple one-liner change, your tests are far too slow. Make the tests so fast you don't have to consider them. A software project will eventually have tens of thousands of unit tests, and team members need to run them all every minute or so without guilt. You do the math.
Thred - https://stackoverflow.com/a/2882456
 Which kind of object methods are valid or not valid for unit testing?
Not all the methods need to be covered in the unit tests, the developers have to decide which method is affecting the application build. We have to test those methods.
 How should we structure a unit test?
This question is kind of project specific.
We follow the basic convention to structure unit tests. We put our tests in the tests folder in the project. And we define the test name as TestFunctionName().
We only test one method in one test function. It helps in avoiding unclarity of failure. Using one method per test helps in identifying which method is actually the nightmare. 
 What kind of assertions should we make in a unit test? 
We just make a single assertion to see if the expected result is similar to the actual result.
If you are making thousands of assertions that is not something you can call a unit test. It is a kind of smoke test. So unit tests just assert the actual and expected of a single method. And even sometimes a single method also needs to divide in components for assertion. 

 
 
The workflow of unit testing:
Creating Test Cases
 The developers write the code in the application for testing the function.
 Reviewing Test Cases
The code is then isolated by the developers to validate the dependencies between the code and other units. This way of isolating the code helps in identifying and eliminating the dependencies.
 
 Baselining Test Cases	
Developers significantly make use of Unit test frameworks or unit testing tools for developing automated test cases.
 
Executing Test Cases
While executing the test cases, the unit test frameworks help to flag and report the failed test cases. Also, based on the failures in the test cases, the unit test frameworks help to stop the related testing.
 
Ways for Unit Testing -

1. Without Using any third party library/framework.

The question that arises here is do we really need a framework. The answer in the first view would be no. 
We can use something like - 

VERIFY(cond) if (!(cond)) {std::cout << "unit test failed at " << __FILE__ << "," << __LINE__; assert(false)}

And then simply create the function for our test.
void CheckBehaviorYWhenXHappens()
{
    VERIFY(expected  != actual);
}

This is a simple home made framework which performs a simple task. Writing your own framework for testing depends on factors like compatibility of other frameworks with your project, the size of the framework is bigger than you need, the framework is acquiring more time than it should.


2. Using any framework 

From the above example it seems like it is trivial to make our own unit testing framework. So why are the unit test frameworks out there so popular? Because there are so many other features of unit test frameworks that are not so easy to implement. A simple example would be automatic collection of test cases. JUnit test framework automatically collects the test starting with the name test, similarly NUnit test framework has Test annotation. This small feature would take a lot of work itself, or we have to use the test in our project with our convention. 
The other problem of using our own framework is that when anyone would start working on it, they will need proper documentation. As the popular unit test frameworks have a proper documentation, so they are easy to pick up by anyone and they are sharable if you have any issue. But in a self made framework we can’t share code.
Ability to run selected test cases, without tweaking code and recompiling. Any decent unit-test framework allows you to specify which tests you want to run on command-line. If you want to debug on unit tests (it's the most important point in them for many developers), you need to be able to select just some to run, without tweaking code all over the place.
I personally like this answer what are benefits of frameworks over writing your own library - 
https://softwareengineering.stackexchange.com/a/137305
But again it is also a matter of requirement. You don’t need to use more features, if there is no requirement for features. And also what if the more features of that framework are of no use for us.



